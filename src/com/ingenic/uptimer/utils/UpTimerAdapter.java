/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingTimer Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.uptimer.utils;

import java.util.List;
import java.util.Map;

import com.ingenic.uptimer.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UpTimerAdapter extends ArrayAdapter<Map<String, Object>> {
    private Context mContext;
    private int mResourceId;
    private Typeface mTf;

    public UpTimerAdapter(Context context, int resource,
            List<Map<String, Object>> objects) {
        super(context, resource, objects);
        mContext = context;
        mResourceId = resource;
        mTf = Typeface.createFromAsset(mContext.getAssets(),"fonts/fzlthjw.ttf");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Map<String, Object> tempMap = getItem(position);
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mResourceId,
                    null);
            viewHolder = new ViewHolder();
            viewHolder.numTextView = (TextView) convertView
                    .findViewById(R.id.num_textView);
            viewHolder.nowTextView = (TextView) convertView
                    .findViewById(R.id.now_textView);
            viewHolder.intervalTextView = (TextView) convertView
                    .findViewById(R.id.interval_textView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.numTextView.setText(tempMap.get("num") + "");
        viewHolder.numTextView.setTypeface(mTf);
        viewHolder.nowTextView.setText(tempMap.get("now") + "");
        viewHolder.nowTextView.setTypeface(mTf);
        viewHolder.intervalTextView.setText(tempMap.get("interval") + "");
        viewHolder.intervalTextView.setTypeface(mTf);

        return convertView;
    }

    class ViewHolder {
        TextView numTextView, nowTextView, intervalTextView;
    }

}

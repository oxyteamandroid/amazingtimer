/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingTimer Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.uptimer.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Application;

/**
 * 该类为android提供的用于静态全局变量的类，主要用于Activity退出后再进入时获取倒计时的相关信息
 *
 * @author ZhanZengYu
 */
public class UptimeGlobalDataApplication extends Application {
    // 获得倒计时的剩余总秒数
    private int mSeconds;

    // 上一次的秒数
    private int mTempSeconds;

    // 状态，控制按钮的字样切换，按钮功能复用
    private boolean mCountEnable;

    // 次数
    private int mCount;

    // ListView数据源
    private List<Map<String, Object>> mUptimeList = new ArrayList<Map<String, Object>>();

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    public int getTempSeconds() {
        return mTempSeconds;
    }

    public void setTempSeconds(int tempSeconds) {
        this.mTempSeconds = tempSeconds;
    }

    public boolean isCountEnable() {
        return mCountEnable;
    }

    public void setCountEnable(boolean countEnable) {
        this.mCountEnable = countEnable;
    }

    public List<Map<String, Object>> getUptimeList() {
        return mUptimeList;
    }

    public void setUptimeList(List<Map<String, Object>> uptimeList) {
        this.mUptimeList = uptimeList;
    }

    public int getSeconds() {
        return mSeconds;
    }

    public void setSeconds(int seconds) {
        this.mSeconds = seconds;
    }
}

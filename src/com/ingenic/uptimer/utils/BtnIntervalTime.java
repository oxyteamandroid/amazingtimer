/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingTimer Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.uptimer.utils;

public class BtnIntervalTime {
    private static long sLastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        // 如果两次点击按钮的时间小于150的话,点击无效
        if (time - sLastClickTime < 150) {
            return true;
        }
        sLastClickTime = time;
        return false;
    }
}

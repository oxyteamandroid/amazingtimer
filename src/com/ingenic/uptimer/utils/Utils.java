package com.ingenic.uptimer.utils;

import com.ingenic.iwds.HardwareList;

public class Utils {
    /**
     * 判断当前的屏是园还是方形
     * @return
     */
    public static boolean IsCircularScreen(){
        return HardwareList.IsCircularScreen();
    }
}

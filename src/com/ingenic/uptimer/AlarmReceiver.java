/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingTimer Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.uptimer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.uptimer.utils.UptimeGlobalDataApplication;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(UpTimerActivity.REPEATING)) {
            UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) context
                    .getApplicationContext();

            if (app.getSeconds() < UpTimerActivity.MAX_UPTIME) {
                // 加10的原因在于AlarmReceiver是100ms接收一次,而计数值是10ms加1
                app.setSeconds(app.getSeconds() + 10);

            } else {
                // 达到最大值时启动Activity并用Toast进行提示
                Intent intentActivity = new Intent(context,
                        UpTimerActivity.class);
                intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentActivity);
                AmazingToast.showToast(context, R.string.max_info,
                        Toast.LENGTH_SHORT);
            }

        }
    }
}

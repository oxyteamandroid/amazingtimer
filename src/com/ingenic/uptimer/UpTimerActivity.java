/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *  Modify:jiabao.nong@ingenic.com
 *  Elf/AmazingTimer Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.uptimer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.BuildOptions;
import com.ingenic.iwds.HardwareList;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.uptimer.utils.BtnIntervalTime;
import com.ingenic.uptimer.utils.UpTimerAdapter;
import com.ingenic.uptimer.utils.UptimeGlobalDataApplication;
import com.ingenic.uptimer.utils.Utils;

public class UpTimerActivity extends RightScrollActivity implements
        OnClickListener {

    // 类似于Action，起区分作用,用于发送消息给AlarmReceiver
    public static final String REPEATING = "com.ingenic.uptimer.repeating";

    // 该值为正计时的最大值,即29:59:59:99
    public static final int MAX_UPTIME = (((((23 * 60) + 59) * 60) + 59) * 100) + 99;

    // 闪烁时间设置
    private final int FLASH_TIME = 1000;

    private final int MSG_REVEAL = 1;
    private final int MSG_FLASH = 2;

    // 右滑进入和进出需要的View
    private RightScrollView mView;

    // 显示计时的历史纪录
    private ListView mUpTimeListView;

    // ListView适配器
    private UpTimerAdapter mUpTimeAdapter;

    // ListView适配器的数据源
    private List<Map<String, Object>> mUpTimeList;

    // 显示正计时的时间
    private TextView mUpTimeTextView;

    // 分别为开始按钮、计时按钮、停止按钮
    private Button mStartButton, mRecordButton, mStopButton, mResetButton;

    // 代表第一界面和第二界面的按钮所在布局
    private LinearLayout mFirstLinear, mSecondLinear;

    // 表示总秒数
    private int mSeconds;
    // 显示的历史纪录条数

    private int mCount = 0;

    // 上一次的秒数，用于计算两次的时间间隔
    private int mTempSeconds = 0;

    // 正计时定时器
    private Timer mTimer = null;

    // 闪烁定时器
    private Timer mFlashTimer = null;

    // 控制TextView间隔闪烁
    private boolean mChange;

    // 用于按钮功能复用时的判断
    private boolean mCountEnable;

    // 控制屏幕是否常亮
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock = null;
    private boolean mScreenisRound = false;
    private Typeface mTf;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_REVEAL:
                    // 数字增1
                    mSeconds++;
                    maxValueOperation();
                    // 更新TextView
                    secondsToReveal(mSeconds);
                    break;
                case MSG_FLASH:
                    // 闪烁
                    if (!mChange) {
                        mUpTimeTextView.setVisibility(View.VISIBLE);
                        mChange = true;
                    } else {
                        mUpTimeTextView.setVisibility(View.INVISIBLE);
                        mChange = false;
                    }
                    break;
                default:
                    break;
            }

        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 以下两句是右滑进入进出动画所需要的
        mView = getRightScrollView();
        mScreenisRound = Utils.IsCircularScreen();
        if (mScreenisRound) {
            mView.setContentView(R.layout.uptimer_round);
            mView.setBackgroundResource(R.drawable.round);
        } else {
            mView.setContentView(R.layout.uptimer);
        }
        mTf = Typeface.createFromAsset(this.getAssets(),"fonts/fzlthjw.ttf");

        // UI控件初始化
        findViews();

        // 设置UI控件
        setViews();

        // 为ListView设置适配器
        adapterSet();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 秒数不为0，说明正计时在启动后退出(这里的退出指使得该Activity返回时会调用onStart()的情况)
        if (getSeconds() != 0) {
            // 恢复数据
            dataRecovery();
            // 达到最大值时
            if (mSeconds >= MAX_UPTIME) {
                // 按钮不可点击
                mStopButton.setClickable(false);
                mCountEnable = true;
                // 显示秒数
                secondsToReveal(MAX_UPTIME);
            } else {
                // false表示之前在后台计时
                if (!isCountEnable()) {
                    // 启动定时器
                    createUpTimer();
                } else {
                    // false表示之前点击了stop按钮
                    // 显示秒数
                    mStartButton.setVisibility(View.VISIBLE);
                    mStopButton.setVisibility(View.GONE);
                    secondsToReveal(mSeconds);
                    // 启用闪烁定时器
                    createFlashTimer();
                }
            }
            // 更新ListView
            mUpTimeAdapter.notifyDataSetChanged();
        } else {
            mStartButton.setVisibility(View.VISIBLE);
            mStopButton.setVisibility(View.GONE);
            mResetButton.setEnabled(false);
            mRecordButton.setEnabled(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 说明计时器已开启且未复位
        if (mSeconds != 0) {
            // 保存数据并启动后台广播
            saveData();
            // 取消定时器
            upTimerCancel();
            // 取消闪烁定时器
            flashTimerCancel();
        } else {
            // 设置秒数为0
            setSeconds(0);
        }
    }

    private void findViews() {
        // TextView初始化
        mUpTimeTextView = (TextView) findViewById(R.id.uptime_textView);

        // Button初始化
        mStartButton = (Button) findViewById(R.id.start_button);
        mRecordButton = (Button) findViewById(R.id.record_button);
        mStopButton = (Button) findViewById(R.id.stop_button);
        mResetButton = (Button) findViewById(R.id.reset_button);

        // 布局初始化
        mFirstLinear = (LinearLayout) findViewById(R.id.firstLinear);
        mSecondLinear = (LinearLayout) findViewById(R.id.secondLinear);

        // ListView初始化
        mUpTimeListView = (ListView) findViewById(R.id.uptimer_listView);
    }

    private void setViews() {
        // 按钮监听
        mStartButton.setOnClickListener(UpTimerActivity.this);
        mRecordButton.setOnClickListener(UpTimerActivity.this);
        mStopButton.setOnClickListener(UpTimerActivity.this);
        mResetButton.setOnClickListener(UpTimerActivity.this);
        mUpTimeTextView.setTypeface(mTf);
        // 获取电源管理器对象
        mPowerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        // 初始化
        mUpTimeList = getUptimeList();
    }

    private void adapterSet() {
        // 创建适配器并匹配到数据上
        mUpTimeAdapter = new UpTimerAdapter(UpTimerActivity.this, R.layout.item,
                mUpTimeList);
        mUpTimeListView.setAdapter(mUpTimeAdapter);
    }

    // 数据恢复
    private void dataRecovery() {
        // 恢复之前所处的状态
        mCountEnable = isCountEnable();
        // 取消后台AlarmReceiver
        mStartButton.setVisibility(View.GONE);
        mStopButton.setVisibility(View.VISIBLE);
        mResetButton.setEnabled(false);
        mRecordButton.setEnabled(true);
        cancelAlarmReceiver();
        // 获取计时的秒数
        mSeconds = getSeconds();
        // 获取计时次数
        mCount = getCount();
        // 获取上一次的秒数
        mTempSeconds = getTempSeconds();
        // 获取历史记录
        mUpTimeList = getUptimeList();
    }

    // 退出前保存数据
    private void saveData() {
        // 保存秒数
        setSeconds(mSeconds);
        // false表示在计时，保存状态
        setCountEnable(mCountEnable);
        // 保存ListView的数据源
        setUptimeList(mUpTimeList);
        // 保存记录次数
        setCount(mCount);
        // 保存上一次的值
        setTempSeconds(mTempSeconds);
        if (!mCountEnable && mSeconds != MAX_UPTIME) {
            // 启动后台服务
            sendToAlarmReceiver();
        }
    }

    // 格式化用，使得输入的为0-9的值变为00-09,大于9的则不变
    public String format(int value) {
        String tmpStr = String.valueOf(value);
        if (value < 10) {
            tmpStr = "0" + tmpStr;
        }
        return tmpStr;
    }

    // 显示正计时剩余时间
    private void secondsToReveal(int seconds) {
        mUpTimeTextView.setVisibility(View.VISIBLE);
        mUpTimeTextView.setText(format(seconds / 60 / 60 / 100) + " : "
                + format(seconds / 60 / 100 % 60) + " : "
                + format(seconds / 100 % 60) + " : " + format(seconds % 100));
    }

    // 将正计时的总数转换为字符串
    private String secondsToString(int seconds) {
        return format(seconds / 60 / 60 / 100) + ":"
                + format(seconds / 60 / 100 % 60) + ":"
                + format(seconds / 100 % 60) + ":" + format(seconds % 100);
    }

    // 启动定时器
    private void createUpTimer() {
        if (mTimer == null) {
            mTimer = new Timer();
            UpTimerTask timerTask = new UpTimerTask();
            mTimer.schedule(timerTask, 10, 10);
            lightOn();
        }
    }

    // 取消定时器
    private void upTimerCancel() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
            lightOff();
        }
    }

    // 启动闪烁定时器
    private void createFlashTimer() {
        if (mFlashTimer == null) {
            mFlashTimer = new Timer();
            FlashTimerTask flashTimerTask = new FlashTimerTask();
            mFlashTimer.schedule(flashTimerTask, FLASH_TIME, FLASH_TIME);
        }
    }

    // 取消闪烁定时器
    private void flashTimerCancel() {
        if (mFlashTimer != null) {
            mFlashTimer.cancel();
            mFlashTimer = null;
        }
        mUpTimeTextView.setVisibility(View.VISIBLE);
    }

    // 计时定时器任务
    class UpTimerTask extends TimerTask {
        @Override
        public void run() {
            mHandler.obtainMessage(MSG_REVEAL).sendToTarget();
        }
    }

    private void maxValueOperation() {
        // 达到最大值时
        if (mSeconds >= MAX_UPTIME) {
            // 取消定时器
            upTimerCancel();
            // 按钮不可点击
            mStopButton.setClickable(false);
            mCountEnable = true;
            AmazingToast.showToast(UpTimerActivity.this, R.string.max_info,
                    AmazingToast.LENGTH_SHORT);

            mSeconds = MAX_UPTIME;
        }
    }

    // 闪烁定时器任务
    class FlashTimerTask extends TimerTask {
        @Override
        public void run() {
            mHandler.removeMessages(MSG_FLASH);
            mHandler.obtainMessage(MSG_FLASH).sendToTarget();
        }

    }

    // 第一界面按钮可见
    private void firstViewButtonEnable() {
        mFirstLinear.setVisibility(View.VISIBLE);
    }

    // 第一界面按钮不可见
    private void firstViewButtonDisable() {
        mFirstLinear.setVisibility(View.GONE);
    }

    // 第二界面可见
    private void secondViewButtonEnable() {
        mSecondLinear.setVisibility(View.VISIBLE);
    }

    // 第二界面不可见
    private void secondViewButtonDisable() {
        mSecondLinear.setVisibility(View.GONE);
    }

    // 以下十个函数为android静态全局变量的操作函数
    // 秒数
    private int getSeconds() {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        return app.getSeconds();
    }

    private void setSeconds(int seconds) {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        app.setSeconds(seconds);
    }

    // 上次秒数
    private int getTempSeconds() {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        return app.getTempSeconds();
    }

    private void setTempSeconds(int tempSeconds) {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        app.setTempSeconds(tempSeconds);
    }

    // 次数
    private int getCount() {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        return app.getCount();
    }

    private void setCount(int count) {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        app.setCount(count);
    }

    // 数据源
    private List<Map<String, Object>> getUptimeList() {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        return app.getUptimeList();
    }

    private void setUptimeList(List<Map<String, Object>> uptimeList) {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        app.setUptimeList(uptimeList);
    }

    // 状态
    private boolean isCountEnable() {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        return app.isCountEnable();
    }

    private void setCountEnable(boolean enable) {
        UptimeGlobalDataApplication app = (UptimeGlobalDataApplication) getApplication();
        app.setCountEnable(enable);
    }

    // 启动后台广播接收器
    private void sendToAlarmReceiver() {
        Intent intent = new Intent(UpTimerActivity.this, AlarmReceiver.class);
        intent.setAction(REPEATING);
        PendingIntent sender = PendingIntent.getBroadcast(UpTimerActivity.this,
                0, intent, 0);

        // 开始时间
        long firstime = SystemClock.elapsedRealtime();

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

        // 100毫秒一个周期，不停的发送广播
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstime + 100,
                1 * 100, sender);
    }

    // 取消后台广播接收器
    private void cancelAlarmReceiver() {
        Intent intent = new Intent(UpTimerActivity.this, AlarmReceiver.class);
        intent.setAction(REPEATING);
        PendingIntent sender = PendingIntent.getBroadcast(UpTimerActivity.this,
                0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.cancel(sender);
    }

    private void resetOperation() {
        // 达到最大值时
        if (mSeconds == MAX_UPTIME) {
            // 可以点击复位按钮复位
            mStopButton.setClickable(true);
        }

        // TextView可见
        mUpTimeTextView.setVisibility(View.VISIBLE);

        // 清除数据
        mUpTimeList.clear();
        // 更新
        mUpTimeAdapter.notifyDataSetChanged();

        // 取消定时器
        upTimerCancel();
        flashTimerCancel();

        // 数据复位
        mSeconds = 0;
        mTempSeconds = 0;
        mCountEnable = false;
        mCount = 0;
        mStopButton.setVisibility(View.GONE);
        mStartButton.setVisibility(View.VISIBLE);
        secondsToReveal(mSeconds);
    }

    @SuppressWarnings("deprecation")
    private void lightOn() {
        if (mWakeLock == null) {
            // 获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
            mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                    | PowerManager.ON_AFTER_RELEASE, "bright");
            // wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK,
            // "bright");
            // 点亮屏幕
            mWakeLock.acquire();
        }
    }

    private void lightOff() {
        // 释放
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    public void onClick(View v) {
        // button如果在指定时间内重复点击，则无效
        if (BtnIntervalTime.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
        // 开始按钮
            case R.id.start_button:
                // firstViewButtonDisable();
                mStartButton.setVisibility(View.GONE);
                mStopButton.setVisibility(View.VISIBLE);
                mResetButton.setEnabled(false);
                mRecordButton.setEnabled(true);
                if (!mCountEnable) {
                    createUpTimer();
                } else {
                    // 取消闪烁定时器
                    flashTimerCancel();
                    // 创建定时器
                    createUpTimer();
                    // TextView可见
                    mUpTimeTextView.setVisibility(View.VISIBLE);
                    mCountEnable = false;
                }
                // secondViewButtonEnable();
                break;
            // 停止/继续按钮
            case R.id.stop_button:
                // 停止->继续（字样转换），停止计时
                mStartButton.setVisibility(View.VISIBLE);
                mStopButton.setVisibility(View.GONE);
                mResetButton.setEnabled(true);
                mRecordButton.setEnabled(false);
                // if (!mCountEnable) {
                // 取消定时器
                upTimerCancel();
                // 启用闪烁定时器
                createFlashTimer();

                // TextView不可见
                mUpTimeTextView.setVisibility(View.INVISIBLE);
                mCountEnable = true;
                break;
            // 记录或者复位按钮
            case R.id.record_button:
                // 记录按钮
                if (!mCountEnable) {
                    // 记录增1
                    mCount++;
                    Map<String, Object> uptimeMap = new HashMap<String, Object>();
                    uptimeMap.put("num", format(mCount));
                    uptimeMap.put("now", secondsToString(mSeconds));
                    uptimeMap.put("interval",
                            secondsToString(mSeconds - mTempSeconds));

                    // 添加数据
                    mUpTimeList.add(uptimeMap);
                    // 更新数据
                    mUpTimeAdapter.notifyDataSetChanged();
                    // 更新上次秒数为当前秒数
                    mTempSeconds = mSeconds;
                }
                break;
            case R.id.reset_button:
                mRecordButton.setEnabled(false);
                mResetButton.setEnabled(false);
                resetOperation();
                break;
            default:
                break;
        }
    }

}
